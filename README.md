# Como desarrollar tus paquetes, y otras cosas maravillosas sin morir en el intento
Este es un pequeño tutorial que hago público basado en la experiencia que he ido
adquiriendo mientras programaba y desarrollaba paquetes. Muchas veces me ha pasado
que para hacer algo he tenido que buscar varias webs introductorias o he tenido que
rondar por stackoverflow recurrentemente para entender mis problemas y ponerles una
solución.

Para evitar que la historia se repita, hago este documento
público para cualquier hispanoparlante que tenga que pasar por lo mismo que yo y
 encuentre esto. Seguramente haya técnicas mejores a las soluciones que propongo, y conforme 
aprenda cosas las iré actualizando. 

El índice de temas de este tutorial es el siguiente (cada tema está en un archivo, al que te lleva el link):
*  [Lo básico: cómo estructurar (bien) un proyecto](/Files/lo-básico-cómo-estructurar-tu-proyecto.md)
*  [Subir un paquete a pip localmente](/Files/subir-un-paquete-a-pip-localmente.md)
*  [Preparar un paquete ejecutable con pip](/Files/preparar-un-paquete-ejecutable-con-pip.md)
*  [Subir un paquete a pip globalmente](/Files/subir-un-paquete-a-pip-globalmente.md)
*  [Subir un paquete a conda localmente](/Files/subir-un-paquete-a-conda-localmente.md)
*  [Subir un paquete a conda globalmente](/Files/subir-un-paquete-a-conda-globalmente.md)
*  [Usar Docker para testear los programas localmente](/Files/usar-docker-para-testear-codigo-localmente.md)
*  [Subir y bajar imágenes de Docker en GitLab](/Files/subir-y-bajar-imagenes-de-docker-en-gitlab.md)
*  [Usar el CI / CD de GitLab](/Files/usar-cicd-de-gitlab-para-testear-codigo-online.md)
*  [Usar Sphinx para documentar código automáticamente](/Files/usar-sphinx-para-documentar-codigo-automaticamente.md)
*  [Implementando la documentación de Sphinx con gitlab Pages](/Files/implementando-la-documentacion-de-sphinx-con-gitlab-cicd.md)
*  [Implementando la documentación de Sphinx con ReadtheDocs](/Files/implementando-la-documentacion-de-sphinx-con-readthedocs.md)
*  [Testeo de código de manera local con pytest]()
*  [Testeo de código en el CI / CD de gitlab]()
*  [El procedimiento recomendado a seguir para publicar un paquete](/Files/el-procedimiento-recomendado-a-seguir-para-publicar-un-paquete.md)
*  [Referencias](/Files/referencias.md)

Mi consejo para realizar el tutorial es que primero **os creéis un proyecto "basura" 
y juguéis con él antes de hacer
nada**; y una vez lo tengáis entendido, deberíais seguir
el procedimiento que (creo) es el apropiado para evitar subir 50 versiones
diferentes. Cada parte del proyecto es independiente, así que repito, os 
recomiendo encarecidamente que leáis cada apartado antes de aventuraros y
fallar.

**DISCLAIMER: Es muy probable que haya mejores maneras de hacer lo que hago. 
Este tutorial busca tener un proyecto medio decente descargable, 
y no algo que no funcione. Si
veis un modo más simple de hacer esto, decídmelo (o haced un merge request) 
y lo corregiré.**

**DISCLAIMER 2: Asumo que ya sabéis usar Git (`add`, `commit`, `push` y `pull` 
por lo menos). Si no sabéis, dedicadle por lo menos unas horas a aprender como
usar git bien y, de aquí hasta el fin de los días, usad git. También asumo
que conocéis los comandos más básicos de bash, como `cd`.**





