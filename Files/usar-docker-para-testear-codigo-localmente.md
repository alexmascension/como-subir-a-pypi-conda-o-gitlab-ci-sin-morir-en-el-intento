# Usar Docker para testear código localmente
<img src="pics/Docker.png"
     alt=""
     style="float: center; left-margin: 12 %"
     width="45 %" />

Docker es una plataforma de contenedores que permite correr imágenes con sistemas operativos o programas
de manera local. Los contenedores son unos objetos un poco diferentes a las máquinas virtuales, como 
Virtual Box, porque hacen un uso diferente de los recursos del ordenador: mientras
que las máquinas virtuales reservan todos los recursos (RAM, espacio, capacidad de
procesamiento, etc.) e instalan el sistema operativo desde el inicio en el que se crea la máquina, 
docker lo hace de manera dinámica, y se aprovecha del sistema operativo huésped.
Grosso modo, los contenedores son más flexibles que las máquinas virtuales, pero
las máquinas virtuales se aislan mejor del sistema operativo padre, de modo que a cambio
de ese coste computacional extra, son más seguras. 

Dicho esto, para el tipo de aplicaciones *al uso* que vamos a testear nos es
más fácil emplear un contenedor a una máquina virtual. Docker tiene
miles de imágenes diferentes (desde sistemas operativos a sistemas con programas 
preinstalados) y, además, permite crear imágenes y publicarlas para que los
demás las bajen.

## Terminología variada
Aunque no es necesario saberlo, nunca viene mal esclarecer conceptos:
*  Imagen: es la "huella dactilar" que tiene toda la información del sistema operativo o, en general, la información del container.
*  Contenedor: es la plataforma, o unidad, donde se lanza la aplicación. Cada contenedor está aislado de otros.
*  Docker Daemon: es el servicio que corre los contenedores y, en general, mantiene toda la estructura y distribución de contendores.
*  Docker Client: es la línea de consola para interactuar con Docker Daemon.
*  Docker Hub: es el registro de imágenes de Docker online. De ahí se puede bajar cualquier imagen que haga falta, o se pueden subir imágenes ya creadas.


Ahora que sabemos la terminología básica, ¡vamos a trabajar con Docker!


## Cómo bajar una imagen y correrla en un contenedor.

El primer paso para usar docker es instalarlo. Las instrucciones detalladas están aquí:

https://docs.docker.com/install/linux/docker-ce/ubuntu/

En el tutorial os indican correr 
```bash
docker install hello-world
```

**Si os da un fallo del estilo** `docker: Got permission denied` **entonces corred el comando con** `sudo`.
Por practicidad, los siguientes comandos irán con `sudo`

Para bajar una imagen hay que correr el código
```bash
sudo docker pull imagen
```

Por ejemplo, si queremos bajar la imagen de miniconda3 de continuumio, sería
```bash
sudo docker pull continuumio/miniconda3
```

Podéis encontrar miles de imágenes en https://hub.docker.com/search?q=&type=image 
y, por lo general, suele poner la línea de comando a usar para ello.

Una vez habéis bajado la imagen la tenéis que correr. Para ello vamos a usar el siguiente comando
```bash
sudo docker run -it continuumio/miniconda3 /bin/bash
```

Esto va a abrir un terminal donde se podrán usar los comandos que necesitéis para
instalar el paquete, y hacer los testeos necesarios. Para entender el comando, vamos
a desmigarlo:
*  `-it` corre Docker para mostrar el contenedor en un terminal. Sin este comando,
   Docker seguirá creando el contenedor, pero lo cerrará sin que se vea nada.
*  `/bin/bash` Nos asegura que se va a abrir una sesión de shell. Por lo general **este argumento no es necesario**, 
   pero de todos modos no viene mal saber que existe. Si en lugar de escribir `/bin/bash` pusiéramos
   otro comando de bash, lo ejecutaría directamente y saldría sin dar aviso.

Una vez abierto el contenedor, podemos hacer todo lo que queramos (hasta podemos hacer
`sudo rm -rf bin` y nuestro ordenador padre está a salvo). Una vez hemos terminado de trabajar en el contenedor,
se puede cerrar la sesión de Docker con `exit` o pulsando <kbd>CTRL</kbd>+<kbd>D</kbd>.

## Historial y borrado de contenedores
Cada vez que corremos un contenedor, se genera una nueva instancia del contenedor, que es identificable con un hash.

Para ver los contenedores que están en curso, se usa
```bash
sudo docker ps
```

Sin embargo, lo normal es que no nos salga ningún docker, porque ya se han ejecutado. Para ver la lista de dockers ejecutados
el comando a usar es
```bash
sudo docker ps -a
```

Un ejemplo de output es:
```bash
CONTAINER ID        IMAGE                    COMMAND                  CREATED             STATUS                      PORTS               NAMES
ed96bb8561e2        continuumio/miniconda3   "/usr/bin/tini -- /b…"   33 minutes ago      Exited (0) 5 minutes ago                        tender_allen
7ea0ea9f245f        continuumio/miniconda3   "/usr/bin/tini -- /b…"   33 minutes ago      Exited (0) 33 minutes ago                       eager_dijkstra
714a3a149ead        hello-world              "/hello"                 5 hours ago         Exited (0) 5 hours ago                          gifted_colden
```

Para borrar un contenedor se usa el comando
```bash
sudo docker rm ID
```

Por ejemplo, si escribimos
```bash
sudo docker rm ed96bb
```

el output sería
```bash
CONTAINER ID        IMAGE                    COMMAND                  CREATED             STATUS                      PORTS               NAMES
7ea0ea9f245f        continuumio/miniconda3   "/usr/bin/tini -- /b…"   33 minutes ago      Exited (0) 33 minutes ago                       eager_dijkstra
714a3a149ead        hello-world              "/hello"                 5 hours ago         Exited (0) 5 hours ago                          gifted_colden
```

El problema es que si hemos corrido muchos contenedores, no vamos a ir borrándolos de uno en uno. Para borrar todos los contenedores de una sentada, el comando es
```bash
sudo docker rm $(sudo docker ps -a -q)
```

El argumento `-q` nos muestra sólo los IDs de los contenedores.

Para eliminar sólo las imagenes que hayan sido cerradas (no borrar una que esté
corriendo), el comando es
```bash
sudo docker container prune
```


## Historial y borrado de imagenes
Al igual que con los contenedores, las imágenes se nos pueden ir acumulando, luego
conviene hacer una gestión eficiente de estas.

Las imágenes se muestran con el comando
```bash
sudo docker images
```

Y las imágenes se borran con 
```bash
sudo docker rmi ID
```

Similarmente, si se quieren borrar todas las imágenes, el comando es
```bash
sudo docker rmi $(sudo docker images -q)
```


## Iniciar un docker cerrado ya existente
Recordemos que cada vez que se emplea el comando `docker run` se crea un nuevo
contenedor desde cero. Si queremos seguir con un contenedor que ya existía, entonces
ejecutamos el comando
```bash
sudo docker start -i ID_contenedor
```


## Correr un contenedor con acceso al ordenador host
Por defecto un contenedor aisla su sistema directorios del ssitema de directorios
del ordenador padre. Sin embargo,
si tenemos que cargar algún archivo o tener acceso a una carpeta local, podemos correr el contenedor con el siguiente comando
```bash
sudo docker run -it -v directorio/host:directorio/docker imagen
```

Por ejemplo, si corremos
```bash
sudo docker run -it -v /media/scripts-para-docker:/media/entrada-docker ubuntu
```

donde `/media/scripts-para-docker/` tiene los archivos `script1.py` y `script2.py`.
Entonces al hacer `ls` en `/media` de docker daría el siguiente árbol:

```
-- media           
  |                     
  ---- entrada-docker     
      |  
      ---- script1.py
      ---- script2.py
```

También podemos plantear la situación inversa: si estoy corriendo un script en el
contenedor que produce unos documentos, y necesito estos documentos en el ordenador 
host, puedo emplear `-v` para crear un directorio puente y exportar los documentos ahí.


## Correr un contenedor con un script
Docker nos ofrece la posibilidad de correr todo aquello que queramos una vez 
hayamos escrito el comando `run`:

```bash
sudo docker run imagen RESTO-DE-COMANDOS
```

Si por ejemplo escribiéramos 

```bash
sudo docker run alexmascension/ubuntu ls
```

nos daría el listado de carpetas dentro de la carpeta raíz del contenedor.

Supongamos que tenemos un script en `home`, llamado `miscript.sh`, si escribimos
el siguiente comando

```bash
sudo docker run alexmascension/ubuntu /home/miscript.sh
```

Entonces nos da un error. ¿Por qué? Porque una vez ejecutado, está buscando `miscript.sh` dentro
del `home` del contenedor, y no existe. Para ello, tenemos que dar acceso puenteando
la dirección del host:

```bash
sudo docker run -v /home:/media  alexmascension/ubuntu /media/miscript.sh
```

Así sí corre el script.

## Compilar y revisar la historia un contenedor personalizado con `docker commit` y `docker history`
Una vez estemos contentos con un contenedor, podemos guardarlo directamente (sin tener que ejecutar `docker start`)
como una imagen. Para ello, seguimos la estructura de `git` y hacemos un commit con el contenedor.

Para crear la imagen el comando a usar es:

```bash
sudo docker commit -m "Mensaje [opcional]" ID_contenedor nombre_imagen
```

Supongamos que hemos hecho unos cambios (añadido la carpeta `Alex`) 
en el contenedor `7ea0ea9f245f`. Entonces
creamos una imagen de docker con el comando

```bash
sudo docker commit -m "Añadida carpeta /alex" 7ea0ea9f245f alexmascension/ubuntu
```

Docker nos devuelve el `SHA256` de la imagen (que coincide con el último commit).
El comando `sudo docker images` nos muestra

```bash
REPOSITORY               TAG                 IMAGE ID            CREATED             SIZE
alexmascension/ubuntu    latest              e6aa622b33e7        11 minutes ago      87.5MB
ubuntu                   latest              20bb25d32758        7 days ago          87.5MB
continuumio/miniconda3   latest              ae46c364060f        3 weeks ago         460MB
hello-world              latest              fce289e99eb9        4 weeks ago         1.84kB
```

Si lo hubieramos guardado como `ubuntu` habríamos sobrescrito la imagen de la repo oficial.

Si no hubieramos guardado ningún nombre, es decir, 
`sudo docker commit 7ea0ea9f245f ` nos habria creado una imagen sin nombre:

```bash
REPOSITORY               TAG                 IMAGE ID            CREATED             SIZE
<none>                   <none>              5ba9dccef1bc        13 seconds ago      87.5MB
alexmascension/ubuntu    latest              e6aa622b33e7        11 minutes ago      87.5MB
```

Podemos consultar el historial de commits de la imagen con el comando
```bash
sudo docker history nombre_imagen
```

En este caso, para `alexmascension/ubuntu` el historial de commits es:
```bash
IMAGE               CREATED             CREATED BY                                      SIZE                COMMENT
e6aa622b33e7        12 minutes ago      /bin/bash                                       21B                 Añadida carpeta /alex
20bb25d32758        7 days ago          /bin/sh -c #(nop)  CMD ["/bin/bash"]            0B                  
<missing>           7 days ago          /bin/sh -c mkdir -p /run/systemd && echo 'do…   7B                  
<missing>           7 days ago          /bin/sh -c rm -rf /var/lib/apt/lists/*          0B                  
<missing>           7 days ago          /bin/sh -c set -xe   && echo '#!/bin/sh' > /…   745B                
<missing>           7 days ago          /bin/sh -c #(nop) ADD file:38a199e521f5e9007…   87.5MB
```

Vemos que la imagen oficial ya presenta unos commits anteriores al que hemos hecho.

Para hacer más commits en la imagen existen dos opciones:
*  Modificar el contenedor que se estaba usando con `sudo docker start` y después del cambio
   hacer un commit con el mismo nombre de imagen.
*  Crear un nuevo contenedor usando `sudo docker run -it alexmascension/ubuntu', y después del cambio
   hacer el commit con el mismo nombre de la imagen.

En ambos casos, la imagen final es la misma. Sin embargo, considerad también que con
el segundo método vais a empezar a crear muchos contenedores, ¡así qué no os olvidéis
de borrarlos!

Por último para esta sección, tenemos opción de correr una imagen en un determinado commit. Para eso basta con correr 
```bash
sudo docker run -it ID_imagen
```

## Publicar una imagen de Docker en Docker Hub
Para publicar una imagen primero hay que logearse en https://hub.docker.com/ y
crear un repositorio (podéis no hacerlo si no queréis), que tenga el mismo nombre
que la imagen que vayáis a subir. Si, por ejemplo, la imagen es `alexmascension/miimagen`, el
nombre del repositorio debería ser `miimagen`. Una vez hecho esto hay que logearse en consola escribiendo 

```bash
sudo docker login
```

Después de meter las credenciales vamos a subir la imagen. Para ello, el primer 
paso (opcional) es taggear la imagen 

```bash
sudo docker tag ID_imagen nombre_imagen:tag
```

como, por ejemplo

```bash
sudo docker tag e6aa622b33e7 alexmascension/ubuntu:version_1
```

Para subir la imagen, siguiendo la terminología de git, se hace un `push`. En
el ejemplo de la imagen anterior:

```bash
sudo docker push alexmascension/ubuntu
```
[Vuelta al inicio](README.md)