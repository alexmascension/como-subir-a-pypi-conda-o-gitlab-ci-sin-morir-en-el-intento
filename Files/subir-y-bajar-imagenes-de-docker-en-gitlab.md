# Subir y bajar imágenes de Docker en GitLab

<img src="pics/Docker-gitlab.png"
     alt=""
     style="float: center; left-margin: 12 %"
     width="55 %" />
     
Esta sección requiere que hayáis leído [Usar Docker para testear los programas localmente](#usar-docker-para-testear-c%C3%B3digo-localmente) antes.
Una vez creadas las imágenes de Docker para testear tus programas en pip/conda, ¿por qué no hacerlas accesibles en GitLab para que el usuario 
se las baje directamente? Para esto existe GitLab Registry, una sección dedicada al almacenamiento de imágenes de Docker. Esta sección 
está accesible en el menú izquierdo, en la sección `Registry`.

Para subir una imagen de Docker, las instrucciones vienen detalladas, aunque
las describimos también aquí. Primero, es necesario que os logeéis en `Registry`

```bash
sudo docker login registry.gitlab.com
```

Una vez registrados, necesitáis crear una nueva imagen con el nombre `registry.gitlab.com/USUARIO/NOMBRE_DE_IMAGEN`.
Si no tenéis la imagen, podéis volver a crear la imagen desde el último contenedor que ejecutásteis, o podéis cambiarle
el nombre directamente usando `docker tag`

```bash
sudo docker tag ID_IMAGEN NUEVO_NOMBRE
```

Por ejemplo, si la lista de imágenes actual es 

```
REPOSITORY                                      TAG                 IMAGE ID            CREATED             SIZE
alexmascension/ubuntu                           latest              238503170c24        2 days ago          87.5MB
<none>                                          <none>              5ba9dccef1bc        2 days ago          87.5MB
```

y quiero cambiar el nombre a `alexmascension/ubuntu` debería escribir

```bash
sudo docker tag 23850 registry.gitlab.com/alexmascension/ubuntu
```

Así, la lista queda:
```
REPOSITORY                                  TAG                 IMAGE ID            CREATED             SIZE
alexmascension/ubuntu                       latest              238503170c24        2 days ago          87.5MB
registry.gitlab.com/alexmascension/ubuntu   latest              238503170c24        2 days ago          87.5MB
<none>                                      <none>              5ba9dccef1bc        2 days ago          87.5MB
```

Para subir la imagen usamos `docker push`
```bash
sudo docker push registry.gitlab.com/alexmascension/ubuntu
```

Para bajar la imagen, usaríamos `docker pull`
```bash
sudo docker pull registry.gitlab.com/alexmascension/ubuntu
```

[Vuelta al inicio](README.md)