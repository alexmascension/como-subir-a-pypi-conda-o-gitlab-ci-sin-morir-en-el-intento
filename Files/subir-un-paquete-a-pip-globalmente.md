# Subir un paquete a pip globalmente

<img src="pics/PyPi-subir.png"
     alt=""
     style="float: center; left-margin: 12 %"
     width="55 %" />

Una vez el paquete está creado, vamos a indicar los pasos para empaquetarlo y
subirlo a Pypi. 

En primer lugar, necesitáis tener una cuenta en Pypi. Registraos en https://pypi.org/account/register/
Existen dos métodos para subir los paquetes.

## Método 1 (usando `twine`): 
Este método no requiere registrar el paquete sino que lo hace twine. Podéis instalar
twine escribiendo 

```bash
pip install twine
```

Después cread el archivo `.tar.gz` de vuestro módulo con el comando

```bash
python setup.py sdist
```

Y subidlo usando

```bash
twine upload dist/*
```

En este paso se os pedirán unas credenciales de acceso, las de PyPi, y listo.

## Método 2 (usando `setup.py`): 
Una vez registrados se crea el registro del paquete.

```bash
python setup.py register
```

y registrad el nombre del paquete.

Después cread el archivo `.tar.gz` de vuestro módulo con el comando

```bash
python setup.py sdist
```

Y subidlo usando

```bash
python setup.py sdist upload
```

¡Listo, ya tenéis vuestro paquete en Pypi! Ahora, un par de consideraciones
*  Cercioraos de que el paquete funciona importando módulos y ejecutando código de
   prueba.
*  Si por alguna razón os habéis equivocado en algo, no podéis borrar el paquete subido a Pypi. 
   Aunque borréis el paquete, Pypi os va a impedir subir el paquete con esa versión, de nuevo,
   luego la tendrás que cambiar. Empezáis a ver con esto que si no estáis seguros de que todo
   funciona corréis el riesgo de acabar con 50 versiones corrigiendo fallos absurdos.


## Subir una nueva versión del paquete
Si habéis empleado el primer método para subir el paquete, y queréis subir una nueva versión, seguid
los siguientes pasos:
  * Borrad las carpetas `build`, `dist` y `.egg-info` del directorio raíz.
  * Cambiad la versión del paquete en `setup.py`, y en el resto de archivos.
  * Repetid los pasos del método 1.


[Vuelta al inicio](README.md)