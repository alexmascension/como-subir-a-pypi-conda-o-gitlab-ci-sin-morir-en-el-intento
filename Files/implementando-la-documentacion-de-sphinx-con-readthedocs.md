
# Implementando la documentación de Sphinx con ReadtheDocs

ReadtheDocs es uno de los *hosts* the documentación más conocidos entre desarrolladores. Para subir la documentación, 
ReadtheDocs usa un entorno donde corre python, sphinx, y otros programas para construir la documentación. De este modo nos 
aseguramos que no existen diferencias entre la documentación de `build` y `source`.

Para implementar la documentación es necesario hacer un par de cambios en el sistema de archivos, de modo que ReadtheDocs 
pueda reconocer los archivos de la documentación.

### `conf.py`
Hay que añadir esta línea en `docs/source/conf.py`:

```python
master_doc = 'index'
```

Esta línea indica que el documento a leer para extraer los contenidos de la documentación es `index.rst`, y no
`contents.rst`, que es el de por defecto.

### `requirements.txt`
El archivo `requirements.txt` se usa para indicar los paquetes que se instalarán por `pip`. En este archivo hay que añadir las 
líneas

```
sphinx
m2r
sphinx-rtd-theme
sphinxcontrib-napoleon
```

`sphinx` es obligatoria, y el resto están descritas en [Usar Sphinx para documentar código automáticamente](/Files/usar-sphinx-para-documentar-codigo-automaticamente.md).

### `.readthedocs.yml`
Este documento incluye la configuración para que el builder de ReadtheDocs pueda construir la documentación. El documento tiene
esta configuración:

```yaml
# Required
version: 2

# Build documentation in the docs/ directory with Sphinx
sphinx:
  configuration: docs/source/conf.py

# Optionally build your docs in additional formats such as PDF and ePub
formats: all

# Optionally set the version of Python and requirements required to build your docs
python:
  version: 3.7
  install:
    - requirements: requirements.txt
```

Este ejemplo es de muestra, con una configuración estándar. Si queréis mirar más opciones, la documentación de 
ReadtheDocs muestra ejemplos con configuraciones más complejas.


## Vincular ReadtheDocs a Gitlab y ejecutar el builder
Una vez todos los documentos han sido creados y pusheados, es hora de actualizar la configuración
de ReadtheDocs. En primer lugar, tienes que hacerte usuario (puedes vincularlo directemente desde gitlab / google), y
tienes que vincularlo a tu cuenta de GitLab.

Una vez hayas creado el usuario y vinculado la cuenta, selecciona el proyecto del que quieras crear la documentación.
Para poder ejecutar el builder y que te lea el archivo `requirements.txt`, tienes que modificar la configuración. 
Vete a la pestaña `Admin`, y en el espacio `Default settings` selecciona el checkbox `Install Project`. De este modo
se instalará el proyecto en local, y además de poder reconocer los archivos `.py` en el import, te aseguras de que el 
proyecto se instala sin errores. 

Una vez hayas aplicado la configuración, en la pestaña `Versions` tienes las versiones de la documentación que se subirán.
La versión `latest`, por defecto, toma la rama `master` para la visualización. También puedes añadir otras ramas del `git`
de tu proyecto si éste las tiene. 

Finalmente, en la pestaña `Builds` podrás ver las builds de la documentación, de modo que si alguna build 
sale mal puedes corregir la raíz del problema.


[Vuelta al inicio](README.md)
