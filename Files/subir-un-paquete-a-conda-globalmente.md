# Subir un paquete a conda globalmente
<img src="pics/Conda-subir.png"
     alt=""
     style="float: center; left-margin: 12 %"
     width="55 %" />

Importante: **Antes de subir el paquete, estad seguros de que funciona en un entorno limpio**

En el `meta.yaml` deberíais tener el apartado
```yaml
test:
  imports:
    - miproyecto
```

Eso debería testear que al importar ``miproyecto`` todos los módulos que declaráis
se importan. Si a la hora de construir el paquete falla este testeo, significa
que os habéis saltado alguna dependencia.

Para subir el paquete a conda, tenéis que registraos en conda (https://anaconda.org/).

Después, si no lo tenéis, instalad el cliente de anaconda escribiendo:
```bash
conda install anaconda-client
```

Logeaos en el cliente escribiendo:
```bash
anaconda login
```

Ahora ya estáis listos para subir tu paquete. Lo podéis hacer escribiendo:
```bash
anaconda upload /home/yoelautor/miniconda/conda-bld/linux-64/miproyecto-1.0-py36_0.tar.bz2
```

La ruta donde se encuentra vuestro archivo aparece al final del mensaje cuando corréis `conda-build miproyecto`. Si lo tenéis hecho de antes
y no os acordáis, volved a compilar el módulo y os dará otra ruta.

[Vuelta al inicio](README.md)