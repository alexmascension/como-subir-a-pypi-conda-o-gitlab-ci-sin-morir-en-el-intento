# Lo básico: cómo estructurar tu proyecto

<img src="pics/Inicio-horror.png"
     alt=""
     style="float: center; left-margin: 12 %"
     width="55 %" />
     
Al principio, la mitad de errores que tenía se debían a que la estructura de 
mi proyecto no era la correcta. Por tanto, esto es **CLAVE** para que los gestores
de paquetes os reconozcan bien los archivos. Si no hacéis esto es muy probable
que construyáis los paquetes y luego no os reconozca nada.

Vamos a suponer dos escenarios: que vuestro proyecto tiene un archivo, o que tiene
varios (siendo uno el principal, o no).
Voy a imaginar que están todos en la carpeta principal tal que así:

```
-- miproyecto           -- miproyecto               -- miproyecto
  |                       |                           |
  ---- principal.py       ---- principal.py           ---- archivo1.py
                          ---- secundario1.py         ---- archivo2.py
                          ---- secundario2.py         ---- archivo3.py
```

Si vuestra situación es esta, vais a tener que reestructurar el proyecto para adecuarlo
al estándar actual. Además, tiene bastante sentido la estructuración de los proyectos que
os voy a proponer. En esta estructuración, tenéis que crear una subcarpeta con el mismo nombre 
(`miproyecto` en este caso)
y crear un archivo `__init__.py`. Este archivo va a ser el principal. 
Cuando hagáis `import miproyecto` en Python, entonces leerá `__init__.py` e importará lo que 
localmente importe `__init__.py`. 

Entonces, dicho esto, existen dos estrategias sobre como manejar los archivos:
*  Si tenéis un solo archivo podéis hacer que el `__init__.py` sea el archivo
   principal. Así, a la hora de hacer `import miproyecto` en Python todas las
   funciones del archivo vienen importadas como `miproyecto.funcion()`.
*  Podéis crear el `__init__.py` vacío e importar los módulos escribiendo 
   `import miproyecto.archivo1`.
*  Dentro del `__init__.py` podéis importar módulos y funciones. Por ejemplo, 
   si tomamos el tercer ejemplo, podéis importar la función `A()` de 
   `archivo1.py` escribiendo `from .archivo1 import A`, o podéis importar el
   módulo `archivo2.py` escribiendo `from . import archivo2`.
   Fijaos en que la importación al uso va sin el punto después del `from`, pero
   para hacer importaciones dentro del archivo `__init__.py` lo tenéis que hacer
   así.

Entonces, la estructura de archivos debería quedar así
```
-- miproyecto           -- miproyecto               -- miproyecto
  |                       |                           |
  ---- miproyecto         ---- miproyecto             ---- miproyecto
      |                       |                           |    
      ---- __init__.py        ---- __init__.py            ---- __init__.py
                              ---- secundario1.py         ---- archivo1.py
                              ---- secundario2.py         ---- archivo2.py
                                                          ---- archivo3.py
```

O, si mantenemos los archivos originales como están:
```
-- miproyecto           -- miproyecto               -- miproyecto
  |                       |                           |
  ---- miproyecto         ---- miproyecto             ---- miproyecto
      |                       |                           |
      ---- __init__.py        ---- __init__.py            ---- __init__.py
      ---- principal.py       ---- principal.py           ---- archivo1.py
                              ---- secundario1.py         ---- archivo2.py
                              ---- secundario2.py         ---- archivo3.py
```

De entre las dos opciones yo recomendaría la última, porque así podeís tener 
el código más organizado. Además, si vuestro archivo principal pasa a ser otro, pero
incluís más importaciones relativas, se vuelve un desbarajuste.

De aquí en adelante vamos a trabajar con el tercer ejemplo (`archivo1/2/3`) por
si tenemos que hacer una mención.

## Lo básico 2: añadir `README.md` y `LICENSE`
En todo proyecto digno tiene que haber un `README.md`, es decir, un fichero que
dé la información básica del módulo, en qué consiste, cómo se puede instalar,
si tiene alguna dependencia o se usa de una manera determinada, etc. La `md` del
archivo implica que está escrito usando Markdown, un tipo de sintaxis que muchas
plataformas lo usan.

`LICENSE`, por otra parte, es un archivo con una licencia de software. Ambos
archivos los podéis redactar manualmente, aunque lo normal es que empleéis, sobre
todo con `LICENSE`, un archivo ya creado. Si tenéis vuestro proyecto albergado en 
GitLab, podeís generar estos archivos automáticamente y, para el archivo `LICENSE`,
incluso podéis elegir el tipo de licencia que más se ajusta a lo que ofrecéis.

Voy a dar por hecho que estos archivos existen para futuras ocasiones en el tutorial.

[Vuelta al inicio](README.md)