# El procedimiento recomendado a seguir para publicar un paquete

<img src="pics/Deployment.png"
     alt=""
     style="float: center; left-margin: 12 %"
     width="55 %" />
     
0.  [Opcional] Documentad bien tu código, formatead bien los documentos, y cread la
    documentación con Sphinx.
1.  Cread el paquete de pip localmente
2.  Instaladlo, desinstaladlo y jugad con las dependencias. 
3.  Intentad instalarlo en un entorno limpio usando docker, y probad que funciona. Si no funciona es que todavía falta alguna dependencia.
4.  Usad el CI/CD para comprobar que funciona en un entorno limpio (y así lo demostráis) en GitLab [si sois usuarios de GitHub, existe Travis-CI, que supongo debería ser parecido].
5.  Si todo va bien, subidlo a Pypi, y os aseguráis de que al menos la gente se puede bajar el paquete.
6.  Haced el paquete de conda a partir de pip, localmente.
7.  Instaladlo en un entorno limpio con docker, y probad si funciona (debería porque el de pip lo hizo).
8.  Usad el CI/CD para comprobar que funciona en un entorno limpio.
9.  Subidlo a conda.
10. Profit (en realidad no porque seguramente sea open source no cobras de ello).
11. [Opcional] subid los contenedores de docker a Registry.
12. [Opcional] subid la documentación de Sphinx a GitLab Pages.

[Vuelta al inicio](README.md)