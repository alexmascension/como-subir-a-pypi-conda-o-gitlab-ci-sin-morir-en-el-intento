# Subir un paquete a pip localmente

<img src="pics/PyPi-PC.png"
     alt=""
     style="float: center; left-margin: 12 %"
     width="55 %" />

Una vez tengáis todo ajustado, viene el turno de hacer el paquete de Pypi. Para
ello, vamos a la carpeta raíz `miproyecto` (la general, no la subcarpeta) y ponemos 
en ella un archivo de python, `setup.py`. Es decir, la estructura de archivos 
tiene que quedar así:
```
-- miproyecto           
  |                     
  ---- miproyecto     
  |    |                  
  |    ---- __init__.py     
  |    ---- archivo1.py
  |    ---- archivo2.py
  |    ---- archivo3.py
  |
  ---- setup.py
  ---- LICENSE
  ---- README.md
```

Este archivo tiene que tener el siguiente contenido:
```python
import setuptools

with open("README.md", 'r') as f:
    long_description = f.read()

setuptools.setup(
   name='miproyecto',
   version='1.0',
   description='miproyecto - un proyecto magnífico y maravilloso',
   long_description=long_description,
   license="LICENSE",
   author='Yo El Autor',
   author_email='micorreo@gmail.com',
   url="https://gitlab.com/yoelautor/miproyecto",
   install_requires=['numpy>=1.14.3', 'pandas>=0.23.4'], 
   packages=setuptools.find_packages(),
   classifiers=[
      "Programming Language :: Python :: 3",
      "License :: OSI Approved :: GNU Library or Lesser General Public License (LGPL)",
      "Operating System :: OS Independent",
   ],
) 
```

Antes de lanzarnos a instalar el paquete, vamos a mirar un par de líneas lo que hacen, para adecuarlas a vuestro paquete.
*  `name` y `version` son el nombre y versión oficiales que van a tener vuestro paquete para internet y durante la importación.
   Por facilidad y lógica, usad el mismo nombre que el de vuestras carpetas, no lo innovéis.
*  Si tenéis el `README.md`, las dos primeras líneas lo leen e incluyen en la descripción. Si no lo tenéis aún podeís quitarlas (y debéis porque si no os va a dar error).
*  Ídem con `license`
*  Ídem con url. 
*  `install_requires` incluye los módulos que necesita vuestro módulo. Completad este 
   apartado con todas dependencias que importe vuestro módulo, o las que necesitéis 
    instalar pero que no estén importadas directamente.
*  `packages` se refiere a archivos de vuestro módulo que tengan que ser importados.
   Con `setuptools.find_packages()` la búsqueda se hace automáticamente.
*  `classifiers` se refiere a características de tu módulo. Podéis encontrar una
   lista extensa aquí https://pypi.org/pypi?%3Aaction=list_classifiers. **Tenéis
   que usar estos clasificadores**, porque si usáis unos cualquesquiera, el módulo no compilará.


## Cuidad bien vuestras dependencias
Meted en la lista de `install_requires` TODAS las dependencias que tengáis. Este
proceso es manual, aunque hay algunos módulos que te pueden ayudar (https://pypi.org/project/findimports/). 
Repito que también necesitáis incluir las dependencias que no se importen. Por ejemplo, 
si vuestro programa necesita `firefox`, tendréis que incluirlo, o tenerlo en mente.

Las dependencias que hay aquí son aquellas que se pueden instalar por Pypi 
(el 99%) de los casos. Si hay algo que no esté en Pypi y que hay instalar,
hay que incluirlo de otra manera; aunque es raro que sea el caso.

Cuando incluís las dependencias **incluid también sus versiones**. Puede parecer
una tontería, pero es un caso muy típico el que vuestro módulo necesite `pandas=0.23`,
el del sistema tenga `pandas=0.20` y que, después de instalar no funcione porque
la versión `0.20` no incluye la función que necesitas. Incluyendo las versiones
fuerzas a Pypi a instalar esa versión y, si no se puede, no instalará el paquete.

[Cerramos paréntesis]

Una vez está el `setup.py` preparado, nos aseguramos que estamos dentro de la carpeta principal y escribimos en consola

```bash
pip install .
```

Entonces `pip` empezará a compilar el paquete. Si todo ha ido bien, deberiáis 
ser capaz de importar el paquete en Python escribiendo `import miproyecto`.
De hecho, haced eso y comprobad que todas las funciones y módulos se importan 
correctamente.

Si queréis desinstalar el paquete, escribid

```bash
pip uninstall miproyecto
```

## Subir un paquetes a pip localmente [más avanzado]. `MANIFEST.in`
Vamos a suponer la siguiente estructura de directorios:
```
-- miproyecto           
  |                     
  ---- miproyecto     
  |    |                  
  |    ---- __init__.py     
  |    ---- archivo1.py
  |    ---- archivo2.py
  |    ---- archivo3.py
  |
  ---- archivos     
  |    |                  
  |    ---- archivo_pesado_1.txt
  |    ---- archivo_pesado_2.txt
  ---- setup.py
  ---- LICENSE
  ---- README.md
```

En este caso tenemos dos archivos `archivo_pesado_1.txt` y `archivo_pesado_2.txt`
que son grandes, pero necesarios en el proyecto.

Si empleamos el método tradicional de subir a pip via `pip install .` es posible
que nos aparezca un fallo del estilo `Could not install packages due to an 
EnvironmentError: [('/ruta-a-archivo/archivo_pesado_1.txt', 
'/tmp/pip-req-build-t4g7kd82/archivo_pesado_1.txt', '[Errno 28] 
No space left on device')`

En otros casos quieres simplemente incluir otros archivos de otras rutas que no
sean la ruta de la carpeta a los archivos de python. En ese caso, es necesario
instalar el paquete de otro modo.

En primer lugar, creamos el archivo `MANIFEST.in` en la raíz (donde se 
encuentrar `setup.py`, `LICENSE` y `README.md`) e incluimos las dos siguientes
líneas:

```
include archivos/archivo_pesado_1.txt
include archivos/archivo_pesado_2.txt
```

Una vez hecho eso, nos vamos con la consola al directorio principal y escribimos

```bash
python setup.py install
```

Se imprimirán unas cuantas cosas en pantallas, entre ellas 
```bash
copying archivos/archivo_pesado_1.txt -> miproyecto-1.0/archivos
copying archivos/archivo_pesado_2.txt -> miproyecto-1.0/archivos
```

En el directorio principal deberían haberse creado dos carpetas extra: `dist` y
`miproyecto.egg-info`. Esas carpetas contienen la información del proyecto, así 
que no las borres.

Si por alguna razón no se han añadido los archivos, añade en el archivo 
`setup.py` una entrada más, llamada `data_files`, que sea una lista con los nombres
de los archivos; y otra entrada, `include_package_data = True`. Debería quedar algo así:

```python
...
install_requires=['numpy>=1.14.3', 'pandas>=0.23.4', 'scikit-image>=0.14.2', 'matplotlib>=3.0.2',
                     'numba>=0.41', 'line_profiler>=2.1.2'],
include_package_data=True,
packages=setuptools.find_packages(),
data_files = ['archivos/archivo_pesado_1.txt', 'archivos/archivo_pesado_2.txt'],
...
```

En teoría, `include_package_data=True` es una linea necesaria para que `setup.py`
lea `MANIFEST.in`, y `data_files` no es necesario, pero por si acaso no viene mal saberlo.

Si, por el contrario, quisiéramos **excluir** los dos archivos, 
en `MANIFEST.in` habría que sustituir `include` por `exclude`, y mantener la línea
`include_package_data=True` en `setup.py`.

Hay otros comando interesantes en `MANIFEST.in` que se pueden usar:
*  recursive-include dir pat1 pat2 incluye todos los archivos que siguen un patrón
   en dir.
*  recursive-exclude es lo mismo, pero excluye.
*  prune dir excluye todos los archivor del directorio `dir`.
*  graft dir incluye todos los archivor del directorio `dir`.





[Vuelta al inicio](README.md)