# Usar Sphinx para documentar código automáticamente

<img src="pics/9928167.png"
     alt=""
     style="float: center; left-margin: 12 %"
     width="18 %" />

Sphinx es un módulo de Python encargado de documentar automáticamente el código 
ya existente, de modo que no tenéis que
dedicar tiempo valioso a escribir un documento sobre vuestro módulo. Además, la **PEP8**
recomienda el uso de Sphinx como módulo de documentación automática, luego nunca
viene mal saber cómo funciona.

El primer paso para la documentación es modificar vuestras funciones y métodos de 
Python para adaptarlos a una estructura legible por Sphinx para crear la documentación.
Un ejemplo de cómo debería documentarse una función es así:

```bash
def suma_listas(a, b):
    '''
    Realiza una suma de dos listas, elemento a elemento.

    :param a: Lista A
    :type a: list
    :param b: Lista B
    :type b: list
    :return: Lista de la suma  de A y B
    :type return: list
    '''

    assert len(a) == len(b)

    return [a[i] + b[i] for i in range(len(a))]
```

O un ejemplo de una clase:

```python
class Casa():
    def __init__(self, n_ventanas, color_tejado, direccion):
        '''
        Clase para una casa, con tres atributos

        :param n_ventanas: Número de ventanas.
        :type n_ventanas: int
        :param color_tejado: Color del tejado.
        :type color_tejado: str
        :param direccion: Dirección (calle y ciudad) de la casa.
        :type direccion: str
        '''
        self.n_ventanas = n_ventanas
        self.color_tejado = color_tejado
        self.direccion = direccion
        self.dueño = ''

    def asignar_dueño(self, dueño):
        '''
        Asigna dueño a la casa

        :param dueño: Dueño (Nombre y Apellido) del dueño de la casa
        :type dueño: str
        '''

        self.dueño = dueño
```

Para ver una forma alternativa de documentar el código y algunas cosas más avanzadas, 
visita https://docs.python-guide.org/writing/documentation/ y 
https://docs.python-guide.org/writing/documentation/.

El lenguaje que Sphinx reconoce se llama reStructuredText (es una palabra, sí) 
y es bastante similar a Markdown. Hay bastante documentación al respecto.

Para poder usar Sphinx, lo primero que hay que hacer es instalarlo usando 

```bash
easy_install -U sphinx
```

o 

```bash
pip install sphinx
```

Una vez instalado, hacemos `cd` hasta la carpeta raíz del proyecto, y creamos una
carpeta para la documentación. En este caso la carpeta se llama `docs`, de modo
que la estructura de directorios queda así.

```
-- miproyecto           
  |                     
  ---- miproyecto     
  |    |                  
  |    ...
  |
  ---- tests
  |    |
  |    ...
  |
  ---- docs
  |    |
  |    ...
  |
  ...
 ```

Una vez en `docs` escribimos

```bash
sphinx-quickstart
``` 

Este comando nos lleva a un programa que construye automáticamente un par de 
documentos. Respondemos de la siguiente manera

```
> Root path for the documentation [.]: 	<ENTER>
> Separate source and build directories (y/N) [n]: 	y
> Name prefix for templates and static dir [_]: 	<ENTER>
> Project name: 	NOMBRE_DEL_PROYECTO
> Author name(s): 	NOMBRE_DE_AUTOR
> Project version: 	VERSION
> Project release [VERSION]: 	<ENTER>
> Source file suffix [.rst]: 	<ENTER>
> Name of your master document (without suffix) [index]: 	<ENTER>
> autodoc: automatically insert docstrings from modules (y/N) [n]: 	y
> doctest: automatically test code snippets in doctest blocks (y/N) [n]: 	n
> intersphinx: link between Sphinx documentation of different projects (y/N) [n]: 	y
> todo: write “todo” entries that can be shown or hidden on build (y/N) [n]: 	n
> coverage: checks for documentation coverage (y/N) [n]: 	n
> pngmath: include math, rendered as PNG images (y/N) [n]: 	n
> jsmath: include math, rendered in the browser by JSMath (y/N) [n]: 	n
> ifconfig: conditional inclusion of content based on config values (y/N) [n]: 	y
> Create Makefile? (Y/n) [y]: 	y
> Create Windows command file? (Y/n) [y]: 	n
```

Una vez hecho esto, en la carpeta `docs` nos aparece la carpeta `build`, que es
donde se creará la documentación, y `source`, que tiene un par de archivos de
configuración. También se genera el archivo `Makefile` que se usa para generar
la documentación final.

Dentro de source está el archivo `conf.py`, que incluye la configuración general
de Sphinx. Para que Sphinx reconozca los archivos de nuestro proyecto, tenemos
que hacer los siguientes cambios:
*  En la línea 41 (aprox.) nos aseguramos que la lista `extensions` contiene el
   string `sphinx.ext.autodoc`. Si no está, lo escribimos.
*  En la líneas 15-17 (aprox.), descomentar 
    ```python
    # import os
    # import sys
    # sys.path.insert(0, os.path.abspath('.'))
    ```
   Con estas líneas referenciamos en Python el directorio donde están nuestros
   archivos [en realidad es el directorio raíz]. 
   La línea de ```sys.path``` debe modificarse para que reconozca el 
   directorio. Se puede hacer poniendo un path absoluto, pero no es lo recomendable.
   Como el directorio raíz está tres directorios arriba del archivo `conf.py`, escribimos 
   ```path
   sys.path.insert(0, os.path.abspath(os.path.join(__file__ ,"../../..")))
   ```

Ahora que sphinx debería reconocer el directorio, escribimos el comando:

```bash
sphinx-apidoc -o source/ ../miproyecto/
```

Este comando busca los archivos python en la carpeta `miproyecto/miproyecto/` y
escribe dos archivos en el directorio `source`:
* `modules.rst` - Contiene un listado de todos los módulos que hay. Si 
  `miproyecto/miproyecto/` tiene más subcarpetas, incluiría varios módulos. En
  este caso sólo está `miproyecto`.
* `miproyecto.rst` - Contiene todos los archivos de la carpeta. Si hay algún
   archivo que no queréis que aparezca, podeís hacer las modificaciones aquí.
   El archivo `__init__.py` aparece dentro de "Module contents" como `miproyecto`, 
   atendiendo a la lógica de los imports de módulos instalados por `pip` y `conda`.
   Si, por ejemplo, quisiéramos sólo importar la función `esta_ordenada()` de
   `archivo3` deberíamos sustituir la línea `.. automodule:: miproyecto.archivo3` por
   `.. automodule:: miproyecto.archivo3.esta_ordenada`.

Lo normal es que la documentación no contenga sólo lo referente a los archivos, 
sino que también se incluya información adicional. Para ello podemos crear documentos
complementarios en `source` y añadir información en formato reStructuredText.
Por ejemplo, vamos a añadir un documento `agradecimientos.rst`:

```
Agradecimientos
===============

Mi proyecto no habría sido viable sin la contribución de Paquito Perales y 
Zutano Zaratán. También me gustaría agradecer a las siguientes entidades:

*  Universidad Excelentísima del Conocimiento Supremo (EUCS)
*  Asociación de Transformadores Lagrangianos Alternativamente Seleccionados (ATLAS)
``` 

Para que Sphinx incluya los módulos y el resto de archivos `rst` que hayamos
creado, necesitamos añadirlos al archivo `index.rst`

Así, por ejemplo, la manera más simple del documento `index.rst` quedaría:
```
Bienvenidos a la documentación de miproyecto!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules
   agradecimientos
```

(fijaos en que `modules` y `agradecimientos` están indentados.)

De este modo, la documentación incluiría el índice de contenidos en la página principal y,
clicando en los módulos, te redigirá a la página de cada módulo, o a los agradecimientos.

Si queréis, por el contrario, que un módulo aparezca en la página principal, habría que escribir

```
.. automodule:: miproyecto.archivo1
   :members:
```

O si queréis que los contenidos de `agradecimientos.rst` aparezcan directamente
en el índice, habría que pegar los contenidos dentro de `index.rst` directamente.

Una vez hemos terminado de modificar todo, escribimos el comando (estando en `docs`)
```bash
make html latexpdf
```

Ahora en `build` deberíamos tener la carpeta `html` con el archivo ` index.html`
y la carpeta `latex` con el archivo `miproyecto.pdf`.

## Customizando la aparencia del html
El formato del html es bastante moñoño, ¡pero puede ser aún mejor! Vamos a implementar un módulo
que cambia la apariencia del html del proyecto para que se parezca al formato de las páginas web
de GitLab (spoiler: **esta sección es interesante si quieres seguir con la siguiente.**), como 
esta: https://sphinx-rtd-theme.readthedocs.io/en/latest/installing.html

Para descargar el tema escribimos en el terminal

```bash
pip install sphinx_rtd_theme
```

y cambiamos la línea 80 (aprox.) del archivo `conf.py` en `docs/source` a

```python
html_theme = "sphinx_rtd_theme"
```

La siguiente vez que corramos `make html` en el terminal (¡recordad hacer 
`make clean` o borrar la carpeta `build/html` antes!) la web debería tener la misma
aparencia.

## Incluyendo archivos (`.txt` y `.md`) en la documentación
En algunas ocasiones nos interesa incluir información en la documentación desde otro documento.
Para ello podemos emplear el comando `.. include` de reStructuredText. El comando se desarrollaría tal que así:

```
.. include:: ../../README.txt
```

Así incluiríamos el texto del archivo `README.txt` en el directorio raíz. 

Si queremos incluir un archivo de markdown, habría que usar el comando `.. mdinclude`:

```
.. mdinclude:: ../../README.md
```

Para poder usar este comando, necesitamos instalar el paquete `m2r` desde PyPi 
con el comando `pip install m2r` y, además, tenemos que añadir el elemento `m2r` a la lista de
extensiones del documento `conf.py`.

```
extensions = ['m2r']
```

## Notación de `numpy`
Una forma un poco más extensa de incluir la información en los docstrings es esta

```python
def suma_listas(a, b):
    '''
    Realiza una suma de dos listas, elemento a elemento.
    
    Esta es una descripción un poco más elaborada. Esta función asume que los
    dos objetos son listas (ergo isinstance(list) = True).
    
    Parameters
    ----------
    a: list
        Lista A
    b: list
        lista B
    
    Returns
    -------
    list
        Lista suma

  
    assert len(a) == len(b)

    return [a[i] + b[i] for i in range(len(a))]
```
Esta notación es la que emplea `numpy` en su documentación. Yo suelo preferir este método de
docstrings. Para que este tipo de documentación sea reconocida, sin embargo, hay que instalar el paquete `sphinxcontrib-napoleon` desde PyPi 
con el comando `pip install sphinxcontrib-napoleon` y, además, tenemos que añadir el elemento `sphinxcontrib.napoleon` a la lista de
extensiones del documento `conf.py`.

```
extensions = ['sphinxcontrib.napoleon']
```

[Vuelta al inicio](README.md)