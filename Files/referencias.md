# Referencias
https://packaging.python.org/tutorials/packaging-projects/

https://python-packaging.readthedocs.io/en/latest/minimal.html

https://conda.io/docs/user-guide/tutorials/build-pkgs-skeleton.html

https://docs.python.org/3/tutorial/modules.html#packages

https://docker-curriculum.com/

https://blog.scalyr.com/2018/09/create-docker-image/

[Vuelta al inicio](README.md)