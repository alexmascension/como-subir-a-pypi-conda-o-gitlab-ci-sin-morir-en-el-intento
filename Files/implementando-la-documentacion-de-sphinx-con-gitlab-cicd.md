

# Implementando la documentación de Sphinx con gitlab Pages

<img src="pics/Sphinx-gitlab.png"
     alt=""
     style="float: center; left-margin: 12 %"
     width="55 %" />
     
GitLab contiene GitLab pages, un sistema de documentación para aquellos módulos que
sean públicos. Así pues, si queréis que vuestra documentación de Sphinx aparezca en GitLab,
en esta sección vamos a ver que es muy sencillo hacerlo.

En primer lugar, haced `push` de la carpeta `docs` al repositorio de GitLab. Una vez
lo hayáis hecho, implementad el siguiente trozo de código en el archivo `.gitlab-ci.yml` 
(si no sabéis de qué estoy hablando, echad un vistazo a la sección [Usar el CI / CD de GitLab](#usar-cicd-de-gitlab-para-testear-c%C3%B3digo-online) )

```bash
pages:
  stage: build
  script:
  - pip install sphinx m2r sphinxcontrib-napoleon sphinx_rtd_theme # Estas son extensiones necesarias en algunos casos
  - cd docs
  - make html
  - cp -r build/html/ ../public/

  artifacts:
    paths:
    - public
 ```

Este trozo de código crea un nuevo test llamado `pages`. Lo que hace el script es
instalar Sphinx y el módulo que instala el tema de Sphinx (`sphinx_rtd_theme`). Al final,
vuelve a compilar el html y copia el resultado de la compilación a una carpeta interna `public`, 
donde aparecerán los resultados de la compilación como una web pública accesible.

Si no queréis que se compile el html, quedaos con estas líneas:
```bash
pages:
  stage: build
  script:
  - cp -r build/html/ ../public/

  artifacts:
    paths:
    - public

  only:
  - master
 ```

El trozo de código `only: - master` es para que sólo se ejecute cuando se esté en la rama `master`.

Una vez hayáis terminado, id a `Settings > Pages`, y ahí tendréis el link público
que podréis distribuir.

Algunas veces Pages puede fallar. Si después de varios intentos de corregir el problema no os funciona nada, 
probad a subir la documentation a ReadtheDocs: [Implementando la documentación de Sphinx con ReadtheDocs](/Files/implementando-la-documentacion-de-sphinx-con-readthedocs.md).

[Vuelta al inicio](README.md)
