# Crear un ejecutable
En muchos casos tenemos que crear un archivo de python que sea ejecutable, 
es decir, que se pueda llamar directamente desde consola. Para hacer que un 
archivo sea ejecutable, hay que hacer dos pasos clave: preparar el archivo
ejecutable para que lo sea y cambiar el `setup.py` para que reconozca el archivo.

## Preparar el ejecutable.
Para preparar el ejecutable, necesitamos que éste incorpore un módulo de interacción
por consola, como `argparse`. Supongamos que el módulo que queremos que sea tiene la siguiente estructura:

```python
# Este módulo es el fichero devuelveSumas.py
import modulo1
import modulo2

def sumaDos(x,y):
  return x+y

def sumaTres(x,y,z):
  return x+y+z


a, b, c = 1, 2, 3
print('Suma 1: ', sumaDos(a,b))
print('Suma 2: ', sumaTres(a,b,c))
```

Nos interesa hacer ejecutable el módulo, para lo cual tenemos que implementar la siguiente estructura:
```python
# Este módulo es el fichero devuelveSumas.py
import modulo1
import modulo2

def sumaDos(x,y):
  return x+y

def sumaTres(x,y,z):
  return x+y+z

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', dest='a')
    parser.add_argument('-b', dest='b')
    parser.add_argument('-c', dest='c')
    args = parser.parse_args()

    a, b, c = args.a, args.b, args.c
    print('Suma 1: ', sumaDos(a,b))
    print('Suma 2: ', sumaTres(a,b,c))

if __name__ == "__main__":
    main()
```

Veis que hemos implementado varios cambios:
  * Empleando `argparse` inicializamos un parser de argumentos, y le añadimos los argumentos de interés (`a`, `b` y `c`), que se pasan como variables empleando el método `parse_args()`. Si esta parte no la entendéis bien os recomiendo que busquéis el módulo `argparse` y miréis unos ejemplos.
  * La parte principal de la función se ha englobado en la función `main()`, que no tiene argumentos.
  * Al final introducimos la línea `if __name__ == "__main__": main()`, que si bien es opcional, nos indica que si ejecutamos el archivo `devuelveSumas.py` directamente, es decir, no lo importamos como módulo de otro módulo principal, entonces corre la función `main()`. Si quitáramos el `if`, y dejáramos que se ejecutase `main()`, entonces se ejecutaría `main()` tanto si importamos `devuelveSumas` desde otro módulo como si lo llamamos directamente desde consola (que es en verdad lo que queremos).

Si hacemos los cambios, entonces podríamos correr el archivo desde consola, escribiendo el comando así:

```bash
python ruta-a-directorio/devuelveSumas.py -a 1 -b 3 -c 5
```

Y nos devolvería

```bash
Suma 1: 4
Suma 2: 9
```

Esto está bien, pero hay un problema: estamos llamando al archivo desde su localización. Por tanto, deberíamos conocer la localización del archivo `devuelveSumas.py` 
cada vez que queramos ejecutarlo, lo cual es bastante engorroso. Por tanto, ya que estamos empaquetando el proyecto entero, vamos a hacer que podamos escribir `devuelveSumas` (sin `.py`!) en el terminal y nos corra `devuelveSumas.py` directamente.

## Modificar el `setup.py` para hacer el módulo ejecutable
El segundo paso es cambiar el archivo `setup.py`. Recapitulamos la estructura de directorios, esta vez con `devuelveSumas.py`

```
-- miproyecto           
  |                     
  ---- miproyecto     
  |    |                  
  |    ---- __init__.py     
  |    ---- archivo1.py
  |    ---- archivo2.py
  |    ---- archivo3.py
  |    ---- devuelveSumas.py
  |
  ---- setup.py
  ---- LICENSE
  ---- README.md
```

Entonces, en el `setup.py` deberíamos introducir un nuevo elemento, `entry_points`, que nos indica qué archivos serán ejecutables. También hemos introducido el elemento `setup_requires`, que en algunos casos es necesario para poder correr la instalación.

Este archivo tiene que tener el siguiente contenido:
```python
import setuptools

with open("README.md", 'r') as f:
    long_description = f.read()

setuptools.setup(
   name='miproyecto',
   version='1.0',
   description='miproyecto - un proyecto magnífico y maravilloso',
   long_description=long_description,
   license="LICENSE",
   author='Yo El Autor',
   author_email='micorreo@gmail.com',
   url="https://gitlab.com/yoelautor/miproyecto",
   install_requires=['numpy>=1.14.3', 'pandas>=0.23.4'], 
   packages=setuptools.find_packages(),
   classifiers=[
      "Programming Language :: Python :: 3",
      "License :: OSI Approved :: GNU Library or Lesser General Public License (LGPL)",
      "Operating System :: OS Independent",
   ],
   setup_requires=[
    'setuptools',
    'setuptools-git',
    'wheel',
   ],
   entry_points={
        'console_scripts': [
            'devuelveSumas = miproyecto.devuelveSumas:main',
            ]
        }
    ) 
```

Vemos que la línea clave para hacer el script ejecutable es `devuelveSumas = miproyecto.devuelveSumas:main`. La parte anterior al `=` es el nombre del script, que será como lo llamemos desde consola. La segunda parte tiene la estructura `X.Y:Z`, donde `X` es la carpeta o el módulo donde está el archivo, en este caso `miproyecto`, `Y` es el nombre del archivo sin `.py`, y `Z` es el nombre de la función a ejecutar. `Z` es **obligatorio** incluirlo, de ahí que en la sección anterior hemos englobado las partes ejecutables dentro de la función `main()`. 

Tendremos que incluir tantos en la lista de `console_scripts` como ficheros ejecutables vayamos a crear.

Una vez esté todo listo, se ejecuta 

```bash
python setup.py install
```

Y si todo se ha instalado sin problemas, podemos ejecutar el programa desde consola:

```bash
devuelveSumas -a 1 -b 3 -c 5
```

Si por alguna razón quieres saber dónde se ha instalado el ejecutable, emplea el comando `which`:

```bash
which devuelveSumas
```

## ¿Y si tengo el archivo en una subcarpeta?
Puede pasar que tu proyecto haya ganado más fuerza y empieces a tener muchos archivos. Quizás en ese caso quieres empezar a guardar los diferentes archivos en más de una carpeta. Vamos a poner la tesitura de que, por cuestiones organizativas, creas una carpeta `consola` y guardas ahí el ejecutable. Es decir, la estructura de archivos pasa a ser esta.

```
-- miproyecto           
  |                     
  ---- miproyecto     
  |    |                  
  |    ---- __init__.py     
  |    ---- archivo1.py
  |    ---- archivo2.py
  |    ---- archivo3.py
  |    |
  |    ---- consola
  |         |
  |         ---- devuelveSumas.py
  |
  ---- setup.py
  ---- LICENSE
  ---- README.md
```

Para poder hacer que el archivo sea ejecutable desde ahí tenemos que hacer un par de modificaciones. En primer lugar, tenemos que añadir un `__init__.py` a `consola` para que la función `main` de `devuelveSumas.py` sea reconocible dentro de la carpeta. Además, también tendremos que modificar el archivo `__init__.py` de la carpeta raíz para que, a su vez, sea reconocible dentro del módulo.

La estructura final de directorios deberia ser así.

```
-- miproyecto           
  |                     
  ---- miproyecto     
  |    |                  
  |    ---- __init__.py     
  |    ---- archivo1.py
  |    ---- archivo2.py
  |    ---- archivo3.py
  |    |
  |    ---- consola
  |         |
  |         ---- __init__.py
  |         ---- devuelveSumas.py
  |
  ---- setup.py
  ---- LICENSE
  ---- README.md
```

```python
# Este es el __init__.py del DIRECTORIO RAÍZ
from . import consola

# ... resto de imports
```

```python
# Este es el __init__.py del DIRECTORIO CONSOLA
from .devuelveSumas import main
```

Lo último que tenemos que hacer es modificar `setup.py` para que reconozca el archivo `devuelveSumas.py` y lo añada a la lista de ejecutables.

```python
# ...
   entry_points={
        'console_scripts': [
            'devuelveSumas = miproyecto.consola.devuelveSumas:main',
            ]
        }
# ...
```

Y, si todo ha ido bien, debería funcionar como antes.

[Vuelta al inicio](README.md)
