# Usar CI/CD de GitLab para testear código online

<img src="pics/CICD.png"
     alt=""
     style="float: center; left-margin: 12 %"
     width="55 %" />
     
CI /CD (Continuous integration / Continuous delivery) es un conjunto de herramientas de
GitLab (en Github es Travis CI) que, cada vez que se hace un commit, evalúa el código 
que tenéis desde cero en un entorno limpio; es decir, va a intentar instalar tu
programa y sus dependencias, ejecutar un test si se pide y, si todo sale 
sin fallos, entonces en vuestro proyecto de GitLab os aparecerá una indicación de que
está todo correctamente elaborado.

Para emplear el CI / CD de gitlab necesitamos crear el archivo `.gitlab-ci.yml` y
ponerlo en el directorio principal. El archivo debería tener un contenido similar
a este [este es un ejemplo de un programa que corro]:

```yaml
image: continuumio/miniconda3:latest

test1:
  stage: build
  script:
    - apt-get update -q -y
    - apt-get install -y build-essential
    - apt-get  --yes --force-yes install libopenmpi2
    - conda env create -f environmentpip.yaml
    - source activate myenv
    - conda install pip
    - pip install mpi4py
    - mpirun --allow-run-as-root -np 2 $(which python) tests/test_runner.py

test2:
  stage: build
  script:
    - apt-get update -q -y
    - apt-get install -y build-essential
    - apt-get  --yes --force-yes install libopenmpi2 
    - conda env create -f environmentconda.yaml
    - source activate myenv
    - mpirun --allow-run-as-root -np 2 $(which python) tests/test_runner.py
```

La primera linea del archivo, `image: `, es opcional. Esta línea sirve para cargar
una imagen de Docker con 
un programa predeterminado. En este caso vamos a instalar cosas por conda, luego 
elegimos una imagen de miniconda3, que sólo tiene Python3, y así nos aseguramos
que las versiones de dependencias por conda funcionan bien. 

Para saber qué imágenes
están disponibles, podéis consultar la web https://hub.docker.com/search?q=&type=image y seleccionar la
que más os convenga. El nombre de la imagen lo podeís encontrar, una vez entrando en el link de la imagen,
buscando el comando de consola. Por ejemplo, si el comando para instalar la imagen es
`docker pull couchbase`, entonces tendríais que escribir `image: couchbase`. 
Si la imagen no es oficial, como en el ejemplo anterior, la tenéis que escribir en
el formato `distribuidor/nombre-de-imagen`. 

El `latest` después de `:` indica el `tag` de la imagen. `latest` siempre bajará
la última versión de la imagen.

Los siguientes blockes de código (`test1` y `test2`) corresponden a los tests que
vamos a correr para evaluar que el código se ejecuta. En cada test tenemos una
cabecera con `stage: build` que es opcional. Esta cabecera nos indica en qué etapa
del CI estamos que, como no es muy relevante para nuestro tipo de proyecto, lo dejamos
en `build`. 

La miga del sistema entra en las líneas de después de `script:`. Cada línea corresponde a 
una instrucción en `bash` para instalar las dependencias. En el primer ejemplo uso `apt-get`, 
`pip` y `conda` para instalar dependencias. Las líneas deberían escribirse tal cual las escribiríais en la consola.

Si durante el proceso vais a usar conda, es recomendable que useis otro archivo `.yaml` para instalar módulos directamente. 
En el archivo `enviromentconda.yaml` el contenido es:

```yaml
name: myenv

channels:
 - conda-forge
 - defaults
 - alexmascension
 
dependencies:
  - python>=3.5
  - bigmpi4py

 ```
 
La línea `myenv:` incluye el nombre del entorno de conda que se carga en la línea `source activate myenv`. 
El bloque de `channels:` incluye los canales usuales (`defaults`) u otros canales
necesarios para instalar los módulos de conda.
 
En la última parte del código se ejecuta un script de python que está en la carpeta `tests`. Fíjate en que todos los directorios y subdirectorios se llaman 
como si hubieras hecho `cd` a la carpeta raíz.

Si os interesa hacer cosas más complejas con el CI / CD, echadle un vistazo a 
https://docs.gitlab.com/ee/ci/README.html; y si queréis ver un ejemplo directamente mirad
https://gitlab.com/gitlab-org/gitlab-ce/blob/master/.gitlab-ci.yml.

Una vez está todo el contenido preparado, haced un commit y subidlo a GitLab. La
estructura de directorios debería quedar así:

```
-- miproyecto           
  |                     
  ---- miproyecto     
  |    |                  
  |    ---- __init__.py     
  |    ---- archivo1.py
  |    ---- archivo2.py
  |    ---- archivo3.py
  |
  ---- tests
  |    |
  |    ---- test_runner.py
  |
  ---- .gitlab-ci.yml
  ---- environmentpip.yaml
  ---- environmentconda.yaml
  ---- setup.py
  ---- LICENSE
  ---- README.md
```

Una vez hayáis hecho el commit, en la página de inicio del proyecto debería aparecer
un símbolo a la izquiera del commit, como en la siguiente imagen.


<img src="pics/general-CI.png"
     alt="CI general scheme"
     style="float: center;" />

Si clicáis en el símbolo o vais a `CI / CD > Pipelines` o `CI / CD > Jobs` entonces
os aparecerá una pantalla donde podréis ver el estado de cada uno de los trabajos, como en la siguiente imagen.

<img src="pics/CI-en-test.png"
     alt="CI general scheme"
     style="float: center;" />

Clicando en el trabajo, aparecerá una terminal donde se ejecutarán todos los comandos que hayáis puesto en `.gitlab-ci.yml`.
Si por alguna razón el trabajo falla, el pipeline terminará ahí y saltará como que no ha terminado de 
ejecutarse. Si esto pasa, **revisa el fallo** y comprobad que se pueda atribuir a 
alguna línea exacta del código. En muchas ocasiones el código puede dar errores inesperados que se solucionan corriendo 
de nuevo el comando, luego es recomendable repetir el 
proceso dándole a `Retry` antes de asumir que hay un fallo en el script.

[Vuelta al inicio](README.md)