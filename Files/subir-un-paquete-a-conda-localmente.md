# Subir un paquete a conda localmente
<img src="pics/Conda-PC.png"
     alt=""
     style="float: center; left-margin: 12 %"
     width="55 %" />

Existen varias maneras de crear un paquete con conda. 
La que he encontrado más sencilla es partiendo de un módulo de Pypi ya existente.
Para ello, vamos a emplear las herramientas `conda skeleton` y `conda-build` 
para construir los paquetes.

En primer lugar hay que posicionarse en una carpeta diferente a la del proyecto, como
`home`. En esa carpeta escribimos

`conda skeleton pypi miproyecto`

Esto bajará los archivos de `miproyecto` desde Pypi, y creará la carpeta `miproyecto` en `home`.
Dentro de la carpeta encontraréis el archivo `meta.yaml`. Es un archivo que incluye 
parámetros de configuración de conda. Este archivo tiene dos puntos principales a revisar: 
las dependecias y el `build`.

Las dependencias son las mismas que las que pusisteis en `setup.py`. Estas dependencias
deberían aparecer en `requirements / run:` y en `requirements / host:`. 
Si no están,
pegadlas como una lista, poniéndo el número de las versiones. En este caso tened en 
cuenta que **las dependencias las va a bajar de dentro de conda**, no de pip. 
Por tanto, si hay algún paquete de conda que se baja desde un canal diferente
(como `bioconda`, `conda-forge` o el nombre de un usuario), hay que incluir ese nombre
con este trozo de código:

```yaml
extra:
  channels:
    - bioconda
    - conda-forge
```

También tened en cuenta que la última versión de un paquete en pip puede ser
diferente a la que haya en conda. Vamos a suponer que necesitamos el paquete `xyz` que se instala 
con el comando `conda install -c userabc xyz`, luego el canal es `userabc`. 
El archivo debería quedar así:

```yaml
{% set name = "miproyecto" %}
{% set version = "1.0" %}

package:
  name: "{{ name|lower }}"
  version: "{{ version }}"

source:
  url: https://pypi.io/packages/source/{{ name[0] }}/{{ name }}/{{ name }}-{{ version }}.tar.gz
  sha256: 432f27484ca3646f00f8337634944bbb67c99f559999900ca028be3c5ccee312

build:
  number: 0
  script: "{{ PYTHON }} -m pip install . --no-deps --ignore-installed -vv "

extra:
  channels:
    - userabc

requirements:
  host:
    - pip
    - numpy >=1.14.3
    - pandas >=0.23.4
    - python >=3.5
    - xyz >=1.2.3
  run:
    - numpy >=1.14.3
    - pandas >=0.23.4
    - python >=3.5
    - xyz >=1.2.3

test:
  imports:
    - miproyecto

about:
  home: https://gitlab.com/yoelautor/miproyecto
  license: GNU Library or Lesser General Public (LGPL)
  license_family: LGPL
  license_file: LICENSE
  summary:miproyecto - un proyecto magnífico y maravilloso

```

**Igual hay partes de aquí que en vuestro yaml no aparecen y viceversa. 
Las nuevas que te aparezcan rellenadlas si creéis que es necesario hacerlo.**

En este archivo aparece este trozo en `build/number`:
```yaml
  script: "{{ PYTHON }} -m pip install . --no-deps --ignore-installed -vv "
```

Si no lo tenéis, podéis completarlo, o bajaos el archivo https://conda.io/docs/_downloads/build1.sh 
de aquí (renombrad `build1.sh` a `build.sh`) y ponedlo en la misma carpeta
donde tenéis `meta.yaml`.

Una vez completado, ejecutad el comando

```bash
conda-build miproyecto
```

Tardará un rato. Si todo ha salido bien, podeís instalar el módulo nuevo
localmente escribiendo
```bash
conda install --use-local miproyecto`
```

y desinstalarlo escribiendo
```bash
conda remove miproyecto
```

[Vuelta al inicio](README.md)