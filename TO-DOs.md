# Quehaceres futuros

### Paquetes en CLI
* Añadir funcArgparse
* Añadir opción de más de una función: estilo funcion-hola y funcion-adios (dos programas separados).
* Añadir opción de más de una función: estilo funcion hola y funcion adios (el mismo programa, pero que el primer argumento sea la función a llamar).

### Codecov
* Crear escenario simple
* Crear escenario con paralelización
* Crear escenario con paralelización + simple
* Explicar el html

### GitLab CI-CD
* Integrar los 3 escenarios de arriba. Para el de paralelización + simple, hacer que se ejecuten múltiples instancias de gitlab por separado. Es decir, hacer un pipe con (1) varias etapas + (2) varios procesos por etapa. Hacer por nombre de job.

### Badges
* Añadir badges como un campeón. La gran mayoría de las que tengo en scallop o así me sirven como modelo.

### Inglés
* La traducción podría ser *How to develop python packages and not die trying*.
* En castellano habría que alterarlo a *Como desarrollar un paquete de python y no morir en el intento* 
* CDPPNMI - siglas de la web?

### Argparse / funcargparse

### Linting y PEP8 con black / flake8

### Pre-commits y git hooks aplicados a linting y PEP8
* Basarme en el código de triku para seleccionar y explicar esos pre-hooks
* Correr no solo en commit, sino pre-commit run
* -a para analizar también files que no están en el stage

### Gitlab-runners
Correr los runners en local. Esto nos puede ser util si queremos correr un trabajo y los
requisitos son superiores a lo que nos proporcionan los shared runners.
* Como instalar un runner https://docs.gitlab.com/runner/
* La localización del runner en este caso es https://docs.gitlab.com/runner/
* Usar las etiquetas para nombrar los runners shared y local.

### Sphinx
* Rediseñar los docs para sphinx porque han cambiado.
* Basarnos en triku para ver cómo hacer la configuración de detección de módulos.
* Subir a readthedocs o a gitlab pages

### Tablas de selección
* Si en un capítulo tenemos la opción de hacer una cosa de varias maneras, en base a dos o más variables de decisión, podemos poner al inicio del tema un árbol de decision / tabla y así que el usuario decida directamente qué es lo que quiere seguir. 
* Las secciones deberían ser lo más independientes entre sí.
